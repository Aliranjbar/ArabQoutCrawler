
package Solr;

import Preprocess.Logging;
import Quotation.QuotMeta;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;


/**
 * 
 * @author Hamed Fakour
 */

public class SolrQuotIndexer {

    
    public void AddQuotDocuments(List<List<QuotMeta>> quot_list) 
    {
        Logging logs = new Logging();
        List<List<QuotMeta>> quots = new ArrayList(quot_list);
        if(quot_list.isEmpty()){
            System.err.println("Urls list is empty!");
            return;
        }
        
        String core_name = "arab_quot";
        HttpSolrClient indexer = new HttpSolrClient("http://185.105.239.181:8983/solr/" + core_name);
        for (List<QuotMeta> quot : quots) {
            for (QuotMeta qu : quot) {
                try {
                    SolrInputDocument document = AddDocument(qu.getID(), qu.getNews_id(), qu.getSource_id(), qu.getPerson_id(), qu.getUrl(),
                    qu.getText(), qu.getPublish_date(), qu.getCrawl_date());
                    indexer.add(document);

                } catch (Exception ex) {
                    System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer.AddQuotDocuments => urls: " + qu.getUrl());
                    Logger.getLogger(SolrQuotIndexer.class.getName()).log(Level.SEVERE, null, ex);
                    continue;
                }
            }
        }
        try {
            indexer.commit(false, true, true);
        } catch (SolrServerException | IOException ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: SolrIndexer.AddQuotDocuments => indexer.final_commit");
            Logger.getLogger(SolrQuotIndexer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    protected SolrInputDocument AddDocument(String id, String news_id, int source_id, String person_id, String url, 
            Set<String> qouts, Date publish_date, Date crawl_date) throws IOException, SolrServerException 
    {                
        SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id", id);
        doc.addField("news_id", news_id);
        doc.addField("person_id", person_id);
        doc.addField("source_id", source_id);
        doc.addField("url", url);
        doc.addField("quot", qouts);
        Logging logs = new Logging();
        doc.addField("pub_date", logs.getUTC_Date(publish_date));
        doc.addField("crawl_date", logs.getUTC_Date(crawl_date));
        return doc;
    }
    

}
