
package Solr;

import Preprocess.Logging;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.util.NamedList;


/**
 *
 * @author h.fakour
 */
public class SolrGetQuery {
    
    public static void main(String[] args) {
        Date now = new Date();
        Logging lo = new Logging();
        String now_utc = lo.getUTC_Date(now);
         
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.MINUTE, -300);
        Date start = cal.getTime();
        String start_utc = lo.getUTC_Date(start); 
        
        SolrGetQuery st = new SolrGetQuery();
        st.ExtractDocuments(start_utc, now_utc);
    }
    
    
    public SolrDocumentList ExtractDocuments(String start_date, String end_date) 
    {
        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.setRows(6000);
        query.setFields("id","url","text","source_id","persons_id","pub_date","crawl_date");
        
        query.set("fq","crawl_date:[" + start_date + " TO " + end_date + "]");
        
        String core = "arab_news";
        HttpSolrClient server = new HttpSolrClient("http://185.105.239.181:8983/solr/" + core);
        QueryResponse response = null;
        try {
            response = server.query(query);
        } catch (SolrServerException | IOException ex) {
            Logger.getLogger(SolrQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
        SolrDocumentList result = new SolrDocumentList();
        if (response.getResults().getNumFound() == 0) {
            return result;
        }
        SolrDocumentList docs = response.getResults();
        for(SolrDocument doc : docs){
            if(doc.size() > 6){
                result.add(doc);
            }
        }
        return result;
    }
    

}
