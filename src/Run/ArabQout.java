
package Run;

import Quotation.ExtractQuotText;
import Preprocess.Logging;
import Quotation.PersonNamesLoad;
import Quotation.QuotMeta;
import QuoteDetector.Quote;
import Solr.SolrGetQuery;
import Solr.SolrQuotIndexer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

/**
 *
 * @author Ali
 */
public class ArabQout {
    
    public static Quote quote; 
    
    public ArabQout(){
        Logging logs = new Logging();
        System.out.println(logs.LoggingStringDate() + " Extract data from Solr server is running... ");
        try {
            PersonNamesLoad.LoadData("resources/PersonsInfo.json");
            if (PersonNamesLoad.PersonsQuot != null || !PersonNamesLoad.PersonsQuot.isEmpty()) {
                quote = new Quote(PersonNamesLoad.PersonsQuot, "resources");
            }
        } catch (Exception ex) {
            System.out.println(logs.LoggingStringDate() + " [Error]: Loading Person Names File ");
            Logger.getLogger(ArabQout.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static void main(String[] args) {
        ArabQout aq = new ArabQout();
        aq.RunScheduler();
    } 
    
     
    public void RunScheduler(){
        Timer timer = new Timer();
        Date start = new Date();
        timer.schedule(new ExtractSolrDocuments(), start, 30*60*1000);
    }
    
    
    class ExtractSolrDocuments extends TimerTask {
        
        @Override
        public void run() {
            Date now = new Date();
            Logging lo = new Logging();
            String now_utc = lo.getUTC_Date(now);
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.MINUTE, -31);
            Date start = cal.getTime();
            String start_utc = lo.getUTC_Date(start);
            
            QoutProcess(start_utc, now_utc);
        }
    }
    
    
    public void QoutProcess(String start_date, String end_date){
        Logging logs = new Logging();
        try {
            SolrGetQuery query = new SolrGetQuery();
            SolrDocumentList documents = query.ExtractDocuments(start_date, end_date);
            System.out.println(logs.LoggingStringDate() + " Extracted Docs => Docs Num: " + documents.size());

            ExtractQuotText eq = new ExtractQuotText();
            List<List<QuotMeta>> quots = eq.getQuots(documents);
            System.out.println(logs.LoggingStringDate() + " Extracted Quots => News num based quots: " + quots.size());

            Random r = new Random();
            int version = r.nextInt(1000);
            System.out.println(logs.LoggingStringDate() + " Start Indexer {" + version + "}");
            SolrQuotIndexer sqi = new SolrQuotIndexer();
            sqi.AddQuotDocuments(quots);
            System.out.println(logs.LoggingStringDate() + " End Indexer {" + version + "}");
        } catch (Exception ex) {
            System.err.println(logs.LoggingStringDate() + " [Error]: Extract Solr Documents ");
            Logger.getLogger(ArabQout.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    
}
