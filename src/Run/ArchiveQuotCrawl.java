

package Run;

import Preprocess.TimeRange;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Ali
 */
public class ArchiveQuotCrawl {
    
    public static void main(String[] args) {
        List<Date> dates = TimeRange.GetDateRange("2018_7_3 07:42:00", "d", 1);
        
        for(int i=0; i<dates.size()-1; i++){
            System.out.println("Num of loop: " + i);
            String start =  TimeRange.getUTC_Date(dates.get(i+1));
            String end = TimeRange.getUTC_Date(dates.get(i));
            
            ArabQout aq = new ArabQout();
            aq.QoutProcess(start, end);
        }
    }
    
}
