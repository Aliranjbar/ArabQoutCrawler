
package Preprocess;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author h.fakour
 */
public class Logging {
    

    public String LoggingStringDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("'['yyyy-MM-dd HH:mm:ss.SSS']'");
        return sdf.format(new Date());
    }
    
    
    public String getUTC_Date(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return formatter.format(date);
    }
    
}
