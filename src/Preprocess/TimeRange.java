
package Preprocess;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
/**
 *
 * @author Hamed.F
 */
public class TimeRange {
    
    public static void main(String[] args) throws ParseException {
        List<Date> dates = GetDateRange("2018_07_03 12:00:00", "d", 1);
        
        for(int i=0; i<dates.size()-1; i++){
            String start =  getUTC_Date(dates.get(i+1));
            String end = getUTC_Date(dates.get(i));
            System.out.println("[" + start + " TO " + end + "]");
        }
    }
    
    public static String getUTC_Date(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return formatter.format(date);
    }
    
    public static List<Date> GetDateRange(String end_date, String duration, int num_section) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        DateFormat formatter = new SimpleDateFormat("yyyy_M_d HH:mm:ss");
        Date  enddate = null; 
        try {
            enddate = (Date)formatter.parse(end_date);
        } catch (ParseException ex) {}
        calendar.setTime(enddate);
        int calendar_duration = Calendar.DATE;
        switch(duration) {
            case "w": calendar_duration = Calendar.WEEK_OF_YEAR; break;
            case "m": calendar_duration = Calendar.MONTH; break;
            case "y": calendar_duration = Calendar.YEAR; break;
            
        }
        for (int i=0; i<num_section; i++) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(calendar_duration, -1);
        }
        //Collections.reverse(dates);
        return dates;
    }
}
