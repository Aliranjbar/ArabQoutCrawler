package Preprocess;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 *
 * @author Hamed.F
 */
public class PythonArabicNormalizer {

    private static String FlaskBaseURL;

    public PythonArabicNormalizer(String python_service_url) {
        FlaskBaseURL = python_service_url;
    }

    public PythonArabicNormalizer() {
        FlaskBaseURL = "http://localhost:5000/normalizer?text=";
    }

    public String getTextNormalized(String text) throws Exception {

         String doc = HTTP_Connection(FlaskBaseURL + URLEncoder.encode(text));
         String docs = FlaskBaseURL + URLEncoder.encode(text);
         //System.out.println(docs);
        JSONObject json = new JSONObject(doc);//.replaceAll("&#34;", "\"")
        //String result = (String) json.get("text");
        //System.out.println("\nResult of Normalizer : " + result);
        return json.get("text").toString();

    }

    public List<String> getTextNormalized(List<String> texts) throws Exception {
        List<String> rst = new ArrayList();
        for (String txt : texts) {
            rst.add(getTextNormalized(txt));
        }
        return rst;
    }

    public Set<String> getTextNormalized(Set<String> texts) throws Exception {
        Set<String> rst = new HashSet();
        for (String txt : texts) {
            rst.add(getTextNormalized(txt));
        }
        return rst;
    }

    public String[] getTextNormalized(String[] texts) throws Exception {
        String[] rst = new String[texts.length];
        for (int i = 0; i < texts.length; i++) {
            rst[i] = getTextNormalized(texts[i]);
        }
        return rst;
    }

    public String HTTP_Connection(String link) throws InterruptedException {
        Logging logs = new Logging();
        String responseBody = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet(link);
            httpget.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 10 * 1000);
            httpget.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 15 * 1000);

            ResponseHandler<String> responseHandler = new ResponseHandler() {
                @Override
                public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        System.err.println(logs.LoggingStringDate() + " [Error]: Normalizer Unexpected response status: " + status);
                        return null;
                    }
                }
            };
            responseBody = httpclient.execute(httpget, responseHandler);
            //System.out.println(doc.text());
        } catch (InterruptedIOException iioe) {
            Thread.sleep(2000);
            System.err.println(logs.LoggingStringDate() + " [TimeOut] => Link: " + link);
        } catch (IOException ex) {
            Logger.getLogger(PythonArabicNormalizer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return responseBody;
    }
}
