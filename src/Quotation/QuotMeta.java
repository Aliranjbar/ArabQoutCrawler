

package Quotation;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.apache.solr.common.SolrDocument;

/**
 *
 * @author Hamed.F
 */
public class QuotMeta {

    private String ID;
    private Set<String> text;
    private String news_id;
    private String person_id;
    private int source_id;
    private String url;
    private Date crawl_date;
    private Date publish_date;
    
    
    public QuotMeta(String person_id, Set<String> quots, SolrDocument doc)
    {
        this.news_id = (String) doc.get("id");
        this.source_id = (int) doc.get("source_id");
        this.person_id = person_id;
        this.ID = person_id + "_" + news_id;
        this.text = new HashSet(quots);
        this.url = (String) doc.get("url");
        this.publish_date = (Date) doc.get("pub_date");
        this.crawl_date = (Date) doc.get("crawl_date");
    }
    
    public String getID() {
        return ID;
    }

    public void setID(String id) {
        this.ID = id;
    }
    
    public Set<String> getText() {
        return text;
    }

    public String getNews_id() {
        return news_id;
    }

    public String getPerson_id() {
        return person_id;
    }

    public int getSource_id() {
        return source_id;
    }

    public String getUrl() {
        return url;
    }

    public Date getCrawl_date() {
        return crawl_date;
    }

    public Date getPublish_date() {
        return publish_date;
    }
    
    public void setText(Set<String> text) {
        this.text = text;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public void setPerson_id(String person_id) {
        this.person_id = person_id;
    }

    public void setSource_id(int source_id) {
        this.source_id = source_id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setCrawl_date(Date crawl_date) {
        this.crawl_date = crawl_date;
    }

    public void setPublish_date(Date publish_date) {
        this.publish_date = publish_date;
    }
}
