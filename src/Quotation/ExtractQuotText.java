
package Quotation;

import Preprocess.Logging;
import Preprocess.PythonArabicNormalizer;
import Quotation.QuotMeta;
import Run.ArabQout;
import static Run.ArabQout.quote;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

/**
 *
 * @author Ali
 */
public class ExtractQuotText {
    
    public List<List<QuotMeta>> getQuots(SolrDocumentList docs){
        Logging logs = new Logging();
        List<List<QuotMeta>> all_quots = new ArrayList();
        for(SolrDocument doc : docs){
            try {
                List<String> texts = (List<String>) doc.get("text");
                PythonArabicNormalizer pan = new PythonArabicNormalizer();
                String text = pan.getTextNormalized(texts.get(0) + " " + texts.get(1) + " " + texts.get(2));
                Map<String, HashSet<String>> quotes = quote.getSpecial(text); 
                List<QuotMeta> quots_meta = new ArrayList();
                for (String pid : quotes.keySet()) {
                    QuotMeta qm = new QuotMeta(pid, quotes.get(pid), doc);
                    quots_meta.add(qm);
                }
                if(!quots_meta.isEmpty()){
                    all_quots.add(quots_meta);
                }
            } catch (Exception ex) {
                System.err.println(logs.LoggingStringDate() + " [Error]: get Quots ");
                Logger.getLogger(ArabQout.class.getName()).log(Level.SEVERE, null, ex);
                continue;
            }
        }
        return all_quots;
    }
    
}
