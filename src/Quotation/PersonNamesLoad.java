
package Quotation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Hamed.F
 */

public class PersonNamesLoad {
    
    public static HashMap<String, HashSet<String>> PersonsQuot;
    
    public static void LoadData(String NamesInfoPath) throws Exception{
        HashMap<String, String> Persons = new HashMap();
        String file_dict = ReadInputStream(NamesInfoPath);
        JSONObject json = new JSONObject(file_dict);
        JSONArray keys = json.names();
        for(int i=0; i<keys.length(); i++){
            Persons.put((String) keys.get(i), json.getString(keys.get(i).toString()));
        }
        
        PersonsQuot = new HashMap();
        for(String name: Persons.keySet()){
            String id = Persons.get(name);
            HashSet<String> tmp = new HashSet();
            if(PersonsQuot.containsKey(id)){
                tmp = new HashSet(PersonsQuot.get(id));
            }
            tmp.add(name);
            PersonsQuot.put(id, tmp);
        }
    }
    
    
    private static String ReadInputStream(String file_dir){
        File in = new File(file_dir);
        String lines = "";
        java.util.Scanner s;
        try {
            s = new java.util.Scanner(in, "UTF-8");
            lines = s.next().replaceAll("[\uFEFF-\uFFFF]", "") + "\n";
            while(s.hasNextLine()){
                lines += s.nextLine() + "\n";
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PersonNamesLoad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lines;
    }
    
    
}
